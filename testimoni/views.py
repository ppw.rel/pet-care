from django.shortcuts import render
from .forms import *
from .models import *


# Create your views here.

def view_testimoni(request):
	if (request.method =="POST"):
		instance = TestimoniForm(request.POST)
		if(instance.is_valid()):
			instance.save()


	testimoni_list = Testimoni.objects.all()
	testimoni_form = TestimoniForm()
	

	context= {
		'testi': testimoni_list,
		'form': testimoni_form,
	}

	return render(request,'view_testimoni.html', context)