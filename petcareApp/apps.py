from django.apps import AppConfig


class PetcareappConfig(AppConfig):
    name = 'petcareApp'
