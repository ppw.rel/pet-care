# Generated by Django 2.2.5 on 2019-10-19 05:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petcareApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FAQModels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=250)),
            ],
        ),
        migrations.DeleteModel(
            name='Schedule',
        ),
    ]
