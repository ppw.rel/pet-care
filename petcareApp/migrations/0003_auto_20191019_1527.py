# Generated by Django 2.2.5 on 2019-10-19 08:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petcareApp', '0002_auto_20191019_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faqmodels',
            name='question',
            field=models.CharField(max_length=500),
        ),
    ]
