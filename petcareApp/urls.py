from django.urls import path
from .views import home

app_name = 'petcareApp'

urlpatterns = [
	path('', home, name='home'),
	]