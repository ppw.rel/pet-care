from django import forms
from . import models

class Question(forms.ModelForm):
	ask_question = forms.CharField(widget=forms.TextInput(attrs={
				"size" : 50,
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Ask Us a Question!",
                }))

	class Meta:
		model = models.FAQModels
		fields = ["ask_question"]