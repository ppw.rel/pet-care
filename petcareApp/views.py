from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import Question
from .models import FAQModels
# Create your views here.

def home(request):
	if request.method == "POST":
		form_question = Question(request.POST)
		if form_question.is_valid():
			form_question.save()
			return redirect('petcareApp:home')
	else:
		form_question = Question()

	questions = FAQModels.objects.all()
	all_question = {
		'form' : form_question,
		'ask' : questions,
	}
	return render(request, 'home.html', all_question)