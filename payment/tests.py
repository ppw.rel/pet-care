from django.test import TestCase, Client
from django.urls import resolve
from .models import Model_form
from .views import *
from .forms import PaymentForm

class TestURLDanView(TestCase):
	def test_url_payments_ada(self):
		response = Client().get('/payments/')
		self.assertEqual(response.status_code, 200)
	def test_template_benar_ga(self):
		response = Client().get('/payments/')
		self.assertTemplateUsed(response,'payment.html')
	def test_pake_fungsi_isi_pembayaran(self):
		fungsi = resolve('/payments/')
		self.assertEqual(fungsi.func, isi_pembayaran)
class TestModel(TestCase):
	@classmethod
	def setUpTestData(cls):
		Model_form.objects.create(nama_pemegang_kartu='Llala lili', ccv='123',
			nomor_kartu_debit='123456789112334',kadaluarsa='2020-12-11')
	def test_create_object(self):
		count = Model_form.objects.all().count()
		self.assertEqual(count,1)
	def test_nama_pemegang_kartu_max_length(self):
		obj = Model_form.objects.get(id=1)
		max_length = obj._meta.get_field('nama_pemegang_kartu').max_length
		self.assertEqual(max_length,50)
	def test_digit_kartu_kredit_max_length(self):			
		obj = Model_form.objects.get(id=1)
		max_length = obj._meta.get_field('nomor_kartu_debit').max_length
		self.assertEqual(max_length,19)
	def test_digit_ccv_max_length(self):
		obj = Model_form.objects.get(id=1)
		max_length = obj._meta.get_field('ccv').max_length
		self.assertEqual(max_length,4)
	def test_nama_pemegang_kartu_sebagai_str(self):
		obj = Model_form.objects.get(id=1)
		expect = f'{obj.nama_pemegang_kartu}'
		self.assertEqual(expect, str(obj))
class TestForm(TestCase):
	def test_apakah_ke_post(self):
		response = self.client.post('/payments/', data={
			'nama_pemegang_kartu':'Llala lili', 'ccv':'123',
			'nomor_kartu_debit':'123456789112334','kadaluarsa':'2020-12-11'
			})
		self.assertEqual(response.status_code, 302)
		form = PaymentForm(data={
			'nama_pemegang_kartu':'Llala lili', 'ccv':'123',
			'nomor_kartu_debit':'123456789112334','kadaluarsa':'2020-12-11'
			})
		self.assertTrue(form.is_valid())
	def test_apakah_gak_ke_post(self):
		form = PaymentForm(data={
			'nama_pemegang_kartu':'Llala lili', 'ccv':'12355',
			'nomor_kartu_debit':'123456789112334','kadaluarsa':'2020-12-11'
			})
		self.assertFalse(form.is_valid())
