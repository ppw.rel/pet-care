# Generated by Django 2.2.6 on 2019-10-19 09:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0005_model_form_time_now'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='model_form',
            name='time_now',
        ),
    ]
