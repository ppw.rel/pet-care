# Generated by Django 2.2.6 on 2019-10-19 04:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0003_auto_20191019_1045'),
    ]

    operations = [
        migrations.AddField(
            model_name='model_form',
            name='id',
            field=models.AutoField(auto_created=True,primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='model_form',
            name='nama_pemegang_kartu',
            field=models.TextField(max_length=50),
        ),
    ]
