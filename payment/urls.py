from django.urls import path
from . import views

app_name = 'payment'

urlpatterns = [
		path('', views.isi_pembayaran, name='isi_pembayaran'),
]