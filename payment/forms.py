from django import forms
from .models import Model_form

class PaymentForm(forms.ModelForm):
	class Meta:
		model = Model_form
		fields = ['nama_pemegang_kartu', 'nomor_kartu_debit', 'kadaluarsa','ccv']
		widgets ={
		'kadaluarsa':forms.DateInput(attrs={'class':'form-control', 
											'type':'date'}),
		'nomor_kartu_debit':forms.NumberInput(attrs={'class':'form-control', 
											'placeholder':"Masukkan nomor kartu debit/kredit Anda"}),
		'nama_pemegang_kartu':forms.TextInput(attrs={'class':'form-control', 
											'placeholder':'Masukkan nama yang tertera di kartu Anda'}),
		'ccv':forms.NumberInput(attrs={'class':'form-control',
										'placeholder':'Masukkan 3-4 digit terakhir dari nomor kartu Anda'})
		}