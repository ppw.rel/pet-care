from django.shortcuts import render, redirect
from .models import Model_form
from .forms import PaymentForm

# Create your views here.
def isi_pembayaran(request):
	if request.method =='POST':
		form = PaymentForm(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.save()
			return redirect('/payments')
	else:
		form = PaymentForm()
	#ambil object terakhir aja
	objects = Model_form.objects.order_by().last()
	isi = {'generate_form': form,
			'all_objects':objects}
	return render(request, 'payment.html', isi)