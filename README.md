"Pet Care"

Nama Anggota :
1. Karin Patricia
2. Marcel Valdano
3. Sinta Bela
4. Siti Khadijah

Link Herokuapp :
petpw.herokuapp.com

Ide Cerita:
Pet Care merupakan sebuah tempat salon hewan yang menyediakan layanan untuk hewan peliharaan. 
Pembuatan pesanan akan dilayani dalam website Pet Care beserta dengan alur pembayarannya.
Kami berharap adanya website ini akan memudahkan para pemilik memanjakan hewan peliharaannya dengan fasilitas yang kami sediakan.

Daftar fitur:
1. Home Page
2. Frequently Asked Questions
3. Pemesanan Layanan
4. Cara pembayaran
5. Testimoni
6. Pesan

[![pipeline status](https://gitlab.com/ppw.rel/pet-care/badges/master/pipeline.svg)]