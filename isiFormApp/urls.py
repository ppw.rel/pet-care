from django.urls import path
from .views import home, isiForm
app_name = 'isiFormApp'

urlpatterns = [
	path('', isiForm, name='isiForm'),
]