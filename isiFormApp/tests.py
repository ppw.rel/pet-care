from django.test import TestCase
from django.test import Client
from .views import *
from .models import *
from .forms import *
from django.urls import resolve


# Create your tests here.
class UnitTest(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_url_isiForm_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    def test_form_status_html(self):
        form = Message_Form()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('id="id_service', form.as_p())
        self.assertIn('id="id_doctor', form.as_p())
        self.assertIn('id="id_fullname', form.as_p())
        self.assertIn('id="id_phone', form.as_p())
        self.assertIn('id="id_date', form.as_p())

    def test_view_isiForm(self):
        found = resolve('/isiForm/')
        self.assertEqual(found.func, isiForm)
    def test_model_Bookings(self): 
        record = Bookings.objects.create(date="2000-09-07", service="nails", doctor="dhafin", fullname="razaafDhafin", phone="999999")
        count_records = Bookings.objects.all().count()
        self.assertEqual(count_records, 1)
