from django.shortcuts import render
from datetime import datetime, date
from .forms import *
from .models import *

# Create your views here.

def home(request):
    response = {}
    return render(request, 'home.html', response)

def isiForm(request):
    form = Message_Form()
    bookings = Bookings.objects.all()
    response = {'message_form': form, 'myBookings' : bookings}
    if request.method == 'POST':
        service = request.POST['service']
        doctor = request.POST['doctor']
        fullname = request.POST['fullname']
        phone = request.POST['phone']
        date = request.POST['date']
        bookings = Bookings(service=service,doctor=doctor,fullname=fullname,phone=phone,date=date)
        bookings.save()

    
    return render(request, 'isiForm.html', response)